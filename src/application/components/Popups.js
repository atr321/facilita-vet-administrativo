import React, { useState, useEffect } from 'react';
import Requests from '../requests/Requests.js';
import * as storage from '../consts/storage.js';
import { useToasts } from 'react-toast-notifications';
import BaseSelect from 'react-select';
import FixRequiredSelect from './FixRequiredSelect';
import '../styles/Popup.css';

const Select = (props) => <FixRequiredSelect {...props} SelectComponent={BaseSelect} options={props.options} defaultValue={props.defaultValue || null} />;

function ConfirmDelete(props) {
  return (
    <div className="popup-container">
      <div className="popup" style={{ minWidth: '360px' }}>
        <div className="popup-header">
          <p>Confirmar</p>
        </div>
        <p className="confirm-popup">Isso irá excluir permanentemente este item.</p>
        <div>
          <button type="button" className="red-btn" style={{ float: 'right' }} onClick={props.deleteFunction}>
            Deletar
          </button>
          <button type="button" className="gray-btn" style={{ float: 'left' }} onClick={props.closePopup}>
            Cancelar
          </button>
        </div>
      </div>
    </div>
  );
}

function AdicionarMedicamento(props) {
  //Opcoes do menu do select
  const [vias, setVias] = useState([]);
  const [frequencias, setFrequencias] = useState([]);
  const [especies, setEspecies] = useState([]);
  const [unidades_medida, setUnidades_medida] = useState([]);

  //Campos do Formulario
  const [nome, setNome] = useState('');
  const [dose_min, setDose_min] = useState('');
  const [dose_max, setDose_max] = useState('');
  const [dose_unica, setDose_unica] = useState('');
  const [concentracao, setConcentracao] = useState('');
  const [selectedVias, setSelectedVias] = useState([]);
  const [selectedFrequencias, setSelectedFrequencias] = useState([]);
  const [especie, setEspecie] = useState({});
  const [unidade_medida, setUnidade_medida] = useState({});

  const { addToast } = useToasts();

  function handleSubmit(event) {
    event.preventDefault();
    let viaIDs = [];
    selectedVias.forEach((item) => {
      viaIDs.push(item.value);
    });

    let frequenciaIDs = [];
    try {
      selectedFrequencias.forEach((item) => {
        frequenciaIDs.push(item.value);
      });
    } catch (err) {
      console.log(err);
    }

    let medicamento = {
      nome: nome,
      dose_min: dose_min,
      dose_max: dose_max,
      dose_unica: dose_unica,
      concentracao: concentracao,
      especie_id: especie.value,
      unidade_medida_id: unidade_medida.value,
      Vias: viaIDs,
      Frequencias: frequenciaIDs,
    };

    Requests.postMedicamento(medicamento, storage.session.getItem('jwtToken'))
      .then((response) => {
        if (response.status === 201) {
          addToast('Medicamento adicionado com sucesso', {
            appearance: 'success',
            autoDismiss: true,
          });
          props.updateMedicamentos();
          props.closePopup();
        } else {
          addToast(response.response.data.errors, {
            appearance: 'error',
            autoDismiss: true,
          });
        }
      })
      .catch(function (err) {
        console.log(err);
      });
  }

  useEffect(() => {
    Requests.getVias().then((data) => {
      setVias(data);
    });
    Requests.getFrequencias().then((data) => {
      setFrequencias(data);
    });
    Requests.getEspecies().then((data) => {
      setEspecies(data);
    });
    Requests.getUnidades_medida().then((data) => {
      setUnidades_medida(data);
    });
  }, []);

  function getSelectOptions(state) {
    let rows = [];
    try {
      state.forEach((item) => {
        let option = { label: item.nome, value: item.id };
        rows.push(option);
      });
    } catch (error) {
      console.log(error);
    }
    return rows;
  }

  function disableDoseMinMaxField() {
    if (document.getElementById('doseUnicaInput').value !== '') {
      document.getElementById('doseMinInput').disabled = true;
      document.getElementById('doseMaxInput').disabled = true;
    } else {
      document.getElementById('doseMinInput').disabled = false;
      document.getElementById('doseMaxInput').disabled = false;
    }
  }

  function disableDoseUnicaField() {
    if (document.getElementById('doseMinInput').value !== '' || document.getElementById('doseMaxInput').value !== '') {
      document.getElementById('doseUnicaInput').disabled = true;
    } else {
      document.getElementById('doseUnicaInput').disabled = false;
    }
  }

  return (
    <div className="popup-container">
      <div className="popup" style={{ maxWidth: '60rem' }}>
        <div className="popup-header">
          <p>Novo Medicamento</p>
        </div>
        <form onSubmit={handleSubmit}>
          <div className="popup-content">
            <div className="inputs-div">
              <div style={{ width: '100%', margin: '0px 8px' }}>
                <p>Nome</p>
                <input type="text" onChange={(e) => setNome(e.target.value)} required />
              </div>
            </div>

            <div className="inputs-div">
              <div className="label-and-input">
                <p>Dose Mín.</p>
                <input
                  type="number"
                  id="doseMinInput"
                  step="any"
                  min="0"
                  onChange={(e) => {
                    setDose_min(e.target.value);

                    disableDoseUnicaField();
                  }}
                />
              </div>

              <div className="label-and-input">
                <p>Dose Max.</p>
                <input
                  type="number"
                  id="doseMaxInput"
                  step="any"
                  min="0"
                  onChange={(e) => {
                    setDose_max(e.target.value);

                    disableDoseUnicaField();
                  }}
                />
              </div>
            </div>

            <div className="inputs-div">
              <div className="label-and-input">
                <p>Dose Única</p>
                <input
                  type="number"
                  id="doseUnicaInput"
                  step="any"
                  min="0"
                  onChange={(e) => {
                    setDose_unica(e.target.value);

                    disableDoseMinMaxField();
                  }}
                />
              </div>

              <div className="label-and-input">
                <p>Concentração</p>
                <input type="number" step="any" min="0" onChange={(e) => setConcentracao(e.target.value)} />
              </div>
            </div>

            <div className="inputs-div">
              <div className="label-and-input">
                <p>Medida da Dose</p>
                <Select
                  className="med-select"
                  placeholder=""
                  options={getSelectOptions(unidades_medida)}
                  onChange={setUnidade_medida}
                  isSearchable={false}
                  styles={customStyles}
                  theme={customTheme}
                  required
                />
              </div>

              <div className="label-and-input">
                <p>Espécie</p>
                <Select className="med-select" placeholder="" options={getSelectOptions(especies)} onChange={setEspecie} isSearchable={false} styles={customStyles} theme={customTheme} required />
              </div>
            </div>

            <div className="inputs-div">
              <div className="label-and-input">
                <p>Vias</p>
                <Select
                  className="med-select"
                  placeholder=""
                  options={getSelectOptions(vias)}
                  onChange={setSelectedVias}
                  isMulti
                  isSearchable={false}
                  styles={customStyles}
                  theme={customTheme}
                  required
                />
              </div>

              <div className="label-and-input">
                <p>Frequências</p>
                <Select
                  className="med-select"
                  placeholder=""
                  options={getSelectOptions(frequencias)}
                  onChange={setSelectedFrequencias}
                  isMulti
                  isSearchable={false}
                  styles={customStyles}
                  theme={customTheme}
                  required
                />
              </div>
            </div>
          </div>
          <div>
            <button type="submit" className="green-btn">
              Salvar
            </button>
          </div>
        </form>
        <button className="gray-btn" onClick={props.closePopup}>
          Cancelar
        </button>
      </div>
    </div>
  );
}

function EditarMedicamento(props) {
  const [showConfirmationPopup, setShowConfirmationPopup] = useState(false);
  //Opcoes do menu do select
  const [vias, setVias] = useState([]);
  const [frequencias, setFrequencias] = useState([]);
  const [especies, setEspecies] = useState([]);
  const [unidades_medida, setUnidades_medida] = useState([]);

  //Campos do Formulario
  const [nome, setNome] = useState(props.medicamento.nome);
  const [dose_min, setDose_min] = useState(props.medicamento.dose_min);
  const [dose_max, setDose_max] = useState(props.medicamento.dose_max);
  const [dose_unica, setDose_unica] = useState(props.medicamento.dose_unica);
  const [concentracao, setConcentracao] = useState(props.medicamento.concentracao);
  const [selectedVias, setSelectedVias] = useState(getSelectOptions(props.medicamento.Vias));
  const [selectedFrequencias, setSelectedFrequencias] = useState(getSelectOptions(props.medicamento.Frequencias));
  const [especie, setEspecie] = useState({
    label: props.medicamento.Especie.nome,
    value: props.medicamento.Especie.id,
  });
  const [unidade_medida, setUnidade_medida] = useState({
    label: props.medicamento.UnidadeMedida.nome,
    value: props.medicamento.UnidadeMedida.id,
  });

  const { addToast } = useToasts();

  useEffect(() => {
    Requests.getVias().then((data) => {
      setVias(data);
    });
    Requests.getFrequencias().then((data) => {
      setFrequencias(data);
    });
    Requests.getEspecies().then((data) => {
      setEspecies(data);
    });
    Requests.getUnidades_medida().then((data) => {
      setUnidades_medida(data);
    });

    if (document.getElementById('doseUnicaInput').value !== '') {
      disableDoseMinMaxField();
    }
    if (document.getElementById('doseMinInput').value !== '') {
      disableDoseUnicaField();
    }
    if (document.getElementById('doseMaxInput').value !== '') {
      disableDoseUnicaField();
    }
  }, []);

  function getSelectOptions(state) {
    let rows = [];
    try {
      state.forEach((item) => {
        let option = { label: item.nome, value: item.id };
        rows.push(option);
      });
    } catch (error) {
      console.log(error);
    }
    return rows;
  }

  function handleSubmit(event) {
    event.preventDefault();
    let viaIDs = [];
    selectedVias.forEach((item) => {
      viaIDs.push(item.value);
    });

    let frequenciaIDs = [];
    try {
      selectedFrequencias.forEach((item) => {
        frequenciaIDs.push(item.value);
      });
    } catch (err) {
      console.log(err);
    }

    let medicamento = {
      id: props.medicamento.id,
      nome: nome,
      dose_min: dose_min,
      dose_max: dose_max,
      dose_unica: dose_unica,
      concentracao: concentracao,
      especie_id: especie.value,
      unidade_medida_id: unidade_medida.value,
      Vias: viaIDs,
      Frequencias: frequenciaIDs,
    };

    Requests.putMedicamento(medicamento, storage.session.getItem('jwtToken'))
      .then((response) => {
        if (response.status === 200) {
          addToast('Medicamento editado com sucesso', {
            appearance: 'success',
            autoDismiss: true,
          });
          props.updateMedicamentos();
          props.closePopup();
        } else {
          addToast(response.response.data.errors, {
            appearance: 'error',
            autoDismiss: true,
          });
        }
      })
      .catch(function (err) {
        console.log(err);
      });
  }

  function handleDelete() {
    Requests.deleteMedicamento(props.medicamento.id, storage.session.getItem('jwtToken'))
      .then((response) => {
        if (response.status === 200) {
          addToast('Medicamento deletado com sucesso', {
            appearance: 'success',
            autoDismiss: true,
          });
          props.updateMedicamentos();
          props.closePopup();
        } else {
          addToast(response.response.data.errors, {
            appearance: 'error',
            autoDismiss: true,
          });
        }
      })
      .catch(function (err) {
        console.log(err);
      });
  }

  function disableDoseMinMaxField() {
    if (document.getElementById('doseUnicaInput').value !== '') {
      document.getElementById('doseMinInput').disabled = true;
      document.getElementById('doseMaxInput').disabled = true;
    } else {
      document.getElementById('doseMinInput').disabled = false;
      document.getElementById('doseMaxInput').disabled = false;
    }
  }

  function disableDoseUnicaField() {
    if (document.getElementById('doseMinInput').value !== '' || document.getElementById('doseMaxInput').value !== '') {
      document.getElementById('doseUnicaInput').disabled = true;
    } else {
      document.getElementById('doseUnicaInput').disabled = false;
    }
  }

  function toggleConfirmDeletePopup() {
    setShowConfirmationPopup(!showConfirmationPopup);
  }

  return (
    <div className="popup-container">
      <div className="popup">
        <div className="popup-header">
          <p>Editar Medicamento</p>
        </div>
        <form onSubmit={handleSubmit}>
          <div className="popup-content">
            <div className="inputs-div">
              <div style={{ width: '100%', margin: '0px 8px' }}>
                <p>Nome</p>
                <input type="text" defaultValue={props.medicamento.nome} onChange={(e) => setNome(e.target.value)} required />
              </div>
            </div>

            <div className="inputs-div">
              <div className="label-and-input">
                <p>Dose Mín.</p>
                <input
                  type="number"
                  id="doseMinInput"
                  step="any"
                  min="0"
                  defaultValue={props.medicamento.dose_min}
                  onChange={(e) => {
                    setDose_min(e.target.value);

                    disableDoseUnicaField();
                  }}
                />
              </div>

              <div className="label-and-input">
                <p>Dose Max.</p>
                <input
                  type="number"
                  id="doseMaxInput"
                  step="any"
                  min="0"
                  defaultValue={props.medicamento.dose_max}
                  onChange={(e) => {
                    setDose_max(e.target.value);

                    disableDoseUnicaField();
                  }}
                />
              </div>
            </div>

            <div className="inputs-div">
              <div className="label-and-input">
                <p>Dose Única</p>
                <input
                  type="number"
                  id="doseUnicaInput"
                  step="any"
                  min="0"
                  defaultValue={props.medicamento.dose_unica}
                  onChange={(e) => {
                    setDose_unica(e.target.value);

                    disableDoseMinMaxField();
                  }}
                />
              </div>

              <div className="label-and-input">
                <p>Concentração</p>
                <input type="number" step="any" min="0" defaultValue={props.medicamento.concentracao} onChange={(e) => setConcentracao(e.target.value)} />
              </div>
            </div>

            <div className="inputs-div">
              <div className="label-and-input">
                <p>Medida da Dose</p>
                <Select
                  className="med-select"
                  placeholder=""
                  options={getSelectOptions(unidades_medida)}
                  onChange={setUnidade_medida}
                  defaultValue={{
                    label: props.medicamento.UnidadeMedida.nome,
                    value: props.medicamento.UnidadeMedida.id,
                  }}
                  isSearchable={false}
                  styles={customStyles}
                  theme={customTheme}
                  required
                />
              </div>

              <div className="label-and-input">
                <p>Espécie</p>
                <Select
                  className="med-select"
                  placeholder=""
                  options={getSelectOptions(especies)}
                  onChange={setEspecie}
                  defaultValue={{
                    label: props.medicamento.Especie.nome,
                    value: props.medicamento.Especie.id,
                  }}
                  isSearchable={false}
                  styles={customStyles}
                  theme={customTheme}
                  required
                />
              </div>
            </div>

            <div className="inputs-div">
              <div className="label-and-input">
                <p>Vias</p>
                <Select
                  className="med-select"
                  placeholder=""
                  options={getSelectOptions(vias)}
                  onChange={setSelectedVias}
                  defaultValue={getSelectOptions(props.medicamento.Vias)}
                  isMulti
                  isSearchable={false}
                  styles={customStyles}
                  theme={customTheme}
                  required
                />
              </div>

              <div className="label-and-input">
                <p>Frequências</p>
                <Select
                  className="med-select"
                  placeholder=""
                  options={getSelectOptions(frequencias)}
                  onChange={setSelectedFrequencias}
                  defaultValue={getSelectOptions(props.medicamento.Frequencias)}
                  isMulti
                  isSearchable={false}
                  styles={customStyles}
                  theme={customTheme}
                  required
                />
              </div>
            </div>
          </div>
          <div>
            <button type="button" className="red-btn" onClick={toggleConfirmDeletePopup}>
              Deletar
            </button>
            <button type="submit" className="green-btn">
              Salvar
            </button>
          </div>
        </form>
        <button className="gray-btn" onClick={props.closePopup}>
          Cancelar
        </button>
      </div>
      {showConfirmationPopup ? <ConfirmDelete deleteFunction={handleDelete} closePopup={toggleConfirmDeletePopup} /> : null}
    </div>
  );
}

function AdicionarRacao(props) {
  //Opcoes do menu do select
  const [especies, setEspecies] = useState([]);
  const [marcas, setMarcas] = useState([]);

  //Campos do Formulario
  const [nome, setNome] = useState('');
  const [energia_metabolizavel, setEnergia_metabolizavel] = useState('');
  const [marca, setMarca] = useState({});
  const [especie, setEspecie] = useState({});

  const { addToast } = useToasts();

  function handleSubmit(event) {
    event.preventDefault();
    let racao = {
      nome: nome,
      energia_metabolizavel: energia_metabolizavel,
      marca_racao_id: marca.value,
      especie_id: especie.value,
    };
    Requests.postRacao(racao, storage.session.getItem('jwtToken'))
      .then((response) => {
        if (response.status === 201) {
          addToast('Ração adicionada com sucesso', {
            appearance: 'success',
            autoDismiss: true,
          });
          props.updateRacoes();
          props.closePopup();
        } else {
          addToast(response.response.data.errors, {
            appearance: 'error',
            autoDismiss: true,
          });
        }
      })
      .catch(function (err) {
        console.log(err);
      });
  }

  useEffect(() => {
    Requests.getEspecies().then((data) => {
      setEspecies(data);
    });
    Requests.getMarcas().then((data) => {
      setMarcas(data);
    });
  }, []);

  function getSelectOptions(state) {
    let rows = [];
    try {
      state.forEach((item) => {
        let option = { label: item.nome, value: item.id };
        rows.push(option);
      });
    } catch (error) {
      console.log(error);
    }
    return rows;
  }

  return (
    <div className="popup-container">
      <div className="popup" style={{ maxWidth: '40rem' }}>
        <div className="popup-header">
          <p>Nova Ração</p>
        </div>
        <form onSubmit={handleSubmit}>
          <div className="popup-content">
            <table className="popup-table">
              <thead>
                <tr className="popup-table-header">
                  <th>Nome</th>
                  <th>Energia Metabolizável</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th>
                    <input type="text" onChange={(e) => setNome(e.target.value)} required />
                  </th>
                  <th>
                    <input type="number" step="any" min="0" onChange={(e) => setEnergia_metabolizavel(Number(e.target.value))} required />
                  </th>
                </tr>
                <tr>
                  <th>
                    <Select placeholder="Espécie" options={getSelectOptions(especies)} onChange={setEspecie} isSearchable={false} styles={customStyles} theme={customTheme} required />
                  </th>
                  <th>
                    <Select placeholder="Marca" options={getSelectOptions(marcas)} onChange={setMarca} isSearchable={false} styles={customStyles} theme={customTheme} required />
                  </th>
                </tr>
              </tbody>
            </table>
          </div>
          <div>
            <button type="submit" className="green-btn">
              Salvar
            </button>
          </div>
        </form>
        <button className="gray-btn" onClick={props.closePopup}>
          Cancelar
        </button>
      </div>
    </div>
  );
}

function EditarRacao(props) {
  const [showConfirmationPopup, setShowConfirmationPopup] = useState(false);
  //Opcoes do menu do select
  const [especies, setEspecies] = useState([]);
  const [marcas, setMarcas] = useState([]);

  //Campos do Formulario
  const [nome, setNome] = useState(props.racao.nome);
  const [energia_metabolizavel, setEnergia_metabolizavel] = useState(props.racao.energia_metabolizavel);
  const [marca, setMarca] = useState({
    label: props.racao.MarcaRacao.nome,
    value: props.racao.MarcaRacao.id,
  });
  const [especie, setEspecie] = useState({
    label: props.racao.Especie.nome,
    value: props.racao.Especie.id,
  });

  const { addToast } = useToasts();

  function handleSubmit(event) {
    event.preventDefault();
    let racao = {
      id: props.racao.id,
      nome: nome,
      energia_metabolizavel: energia_metabolizavel,
      marca_racao_id: marca.value,
      especie_id: especie.value,
    };

    Requests.putRacao(racao, storage.session.getItem('jwtToken'))
      .then((response) => {
        if (response.status === 200) {
          addToast('Ração editada com sucesso', {
            appearance: 'success',
            autoDismiss: true,
          });
          props.updateRacoes();
          props.closePopup();
        } else {
          addToast(response.response.data.errors, {
            appearance: 'error',
            autoDismiss: true,
          });
        }
      })
      .catch(function (err) {
        console.log(err);
      });
  }

  function toggleConfirmDeletePopup() {
    setShowConfirmationPopup(!showConfirmationPopup);
  }

  function handleDelete() {
    Requests.deleteRacao(props.racao.id, storage.session.getItem('jwtToken'))
      .then((response) => {
        if (response.status === 200) {
          addToast('Ração deletada com sucesso', {
            appearance: 'success',
            autoDismiss: true,
          });
          props.updateRacoes();
          props.closePopup();
        } else {
          addToast(response.response.data.errors, {
            appearance: 'error',
            autoDismiss: true,
          });
        }
      })
      .catch(function (err) {
        console.log(err);
      });
  }

  useEffect(() => {
    Requests.getEspecies().then((data) => {
      setEspecies(data);
    });
    Requests.getMarcas().then((data) => {
      setMarcas(data);
    });
  }, []);

  function getSelectOptions(state) {
    let rows = [];
    try {
      state.forEach((item) => {
        let option = { label: item.nome, value: item.id };
        rows.push(option);
      });
    } catch (error) {
      console.log(error);
    }
    return rows;
  }

  return (
    <div className="popup-container">
      <div className="popup" style={{ maxWidth: '40rem' }}>
        <div className="popup-header">
          <p>Editar Ração</p>
        </div>
        <form onSubmit={handleSubmit}>
          <div className="popup-content">
            <table className="popup-table">
              <thead>
                <tr className="popup-table-header">
                  <th>Nome</th>
                  <th>Energia Metabolizável</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th>
                    <input type="text" defaultValue={props.racao.nome} onChange={(e) => setNome(e.target.value)} required />
                  </th>
                  <th>
                    <input type="number" step="any" min="0" defaultValue={props.racao.energia_metabolizavel} onChange={(e) => setEnergia_metabolizavel(Number(e.target.value))} required />
                  </th>
                </tr>
                <tr>
                  <th>
                    <Select
                      placeholder="Espécie"
                      options={getSelectOptions(especies)}
                      onChange={setEspecie}
                      defaultValue={{
                        label: props.racao.Especie.nome,
                        value: props.racao.Especie.id,
                      }}
                      isSearchable={false}
                      styles={customStyles}
                      theme={customTheme}
                      required
                    />
                  </th>
                  <th>
                    <Select
                      placeholder="Marca"
                      options={getSelectOptions(marcas)}
                      onChange={setMarca}
                      defaultValue={{
                        label: props.racao.MarcaRacao.nome,
                        value: props.racao.MarcaRacao.id,
                      }}
                      isSearchable={false}
                      styles={customStyles}
                      theme={customTheme}
                      required
                    />
                  </th>
                </tr>
              </tbody>
            </table>
          </div>
          <div>
            <button type="button" className="red-btn" onClick={toggleConfirmDeletePopup}>
              Deletar
            </button>
            <button type="submit" className="green-btn">
              Salvar
            </button>
          </div>
        </form>
        <button className="gray-btn" onClick={props.closePopup}>
          Cancelar
        </button>
      </div>
      {showConfirmationPopup ? <ConfirmDelete deleteFunction={handleDelete} closePopup={toggleConfirmDeletePopup} /> : null}
    </div>
  );
}

function AdicionarFrequencia(props) {
  //Campos do Formulario
  const [nome, setNome] = useState('');

  const { addToast } = useToasts();

  function handleSubmit(event) {
    event.preventDefault();
    let frequencia = {
      nome: nome,
    };
    Requests.postFrequencia(frequencia, storage.session.getItem('jwtToken'))
      .then((response) => {
        if (response.status === 201) {
          addToast('Frequencia adicionada com sucesso', {
            appearance: 'success',
            autoDismiss: true,
          });
          props.updateFrequencias();
          props.closePopup();
        } else {
          addToast(response.response.data.errors, {
            appearance: 'error',
            autoDismiss: true,
          });
        }
      })
      .catch(function (err) {
        console.log(err);
      });
  }

  return (
    <div className="popup-container">
      <div className="popup" style={{ maxWidth: '40rem' }}>
        <div className="popup-header">
          <p>Nova Frequencia</p>
        </div>
        <form onSubmit={handleSubmit}>
          <div className="popup-content">
            <table className="popup-table">
              <thead>
                <tr className="popup-table-header">
                  <th>Nome</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th>
                    <input type="text" onChange={(e) => setNome(e.target.value)} required />
                  </th>
                </tr>
              </tbody>
            </table>
          </div>
          <div>
            <button type="submit" className="green-btn">
              Salvar
            </button>
          </div>
        </form>
        <button className="gray-btn" onClick={props.closePopup}>
          Cancelar
        </button>
      </div>
    </div>
  );
}

function EditarFrequencia(props) {
  const [showConfirmationPopup, setShowConfirmationPopup] = useState(false);
  //Campos do Formulario
  const [nome, setNome] = useState(props.frequencia.nome);

  const { addToast } = useToasts();

  function handleSubmit(event) {
    event.preventDefault();
    let frequencia = {
      id: props.frequencia.id,
      nome: nome,
    };

    Requests.putFrequencia(frequencia, storage.session.getItem('jwtToken'))
      .then((response) => {
        if (response.status === 200) {
          addToast('Frequencia editada com sucesso', {
            appearance: 'success',
            autoDismiss: true,
          });
          props.updateFrequencias();
          props.closePopup();
        } else {
          addToast(response.response.data.errors, {
            appearance: 'error',
            autoDismiss: true,
          });
        }
      })
      .catch(function (err) {
        console.log(err);
      });
  }

  function toggleConfirmDeletePopup() {
    setShowConfirmationPopup(!showConfirmationPopup);
  }

  function handleDelete() {
    Requests.deleteFrequencia(props.frequencia.id, storage.session.getItem('jwtToken'))
      .then((response) => {
        if (response.status === 200) {
          addToast('Frequencia deletada com sucesso', {
            appearance: 'success',
            autoDismiss: true,
          });
          props.updateFrequencias();
          props.closePopup();
        } else {
          addToast(response.response.data.errors, {
            appearance: 'error',
            autoDismiss: true,
          });
        }
      })
      .catch(function (err) {
        console.log(err);
      });
  }

  return (
    <div className="popup-container">
      <div className="popup" style={{ maxWidth: '40rem' }}>
        <div className="popup-header">
          <p>Editar Frequencia</p>
        </div>
        <form onSubmit={handleSubmit}>
          <div className="popup-content">
            <table className="popup-table">
              <thead>
                <tr className="popup-table-header">
                  <th>Nome</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th>
                    <input type="text" defaultValue={props.frequencia.nome} onChange={(e) => setNome(e.target.value)} required />
                  </th>
                </tr>
              </tbody>
            </table>
          </div>
          <div>
            <button type="button" className="red-btn" onClick={toggleConfirmDeletePopup}>
              Deletar
            </button>
            <button type="submit" className="green-btn">
              Salvar
            </button>
          </div>
        </form>
        <button className="gray-btn" onClick={props.closePopup}>
          Cancelar
        </button>
      </div>
      {showConfirmationPopup ? <ConfirmDelete deleteFunction={handleDelete} closePopup={toggleConfirmDeletePopup} /> : null}
    </div>
  );
}

function AdicionarVia(props) {
  //Campos do Formulario
  const [nome, setNome] = useState('');

  const { addToast } = useToasts();

  function handleSubmit(event) {
    event.preventDefault();
    let via = {
      nome: nome,
    };
    Requests.postVia(via, storage.session.getItem('jwtToken'))
      .then((response) => {
        if (response.status === 201) {
          addToast('Via adicionada com sucesso', {
            appearance: 'success',
            autoDismiss: true,
          });
          props.updateVias();
          props.closePopup();
        } else {
          addToast(response.response.data.errors, {
            appearance: 'error',
            autoDismiss: true,
          });
        }
      })
      .catch(function (err) {
        console.log(err);
      });
  }

  return (
    <div className="popup-container">
      <div className="popup" style={{ maxWidth: '40rem' }}>
        <div className="popup-header">
          <p>Nova Via</p>
        </div>
        <form onSubmit={handleSubmit}>
          <div className="popup-content">
            <table className="popup-table">
              <thead>
                <tr className="popup-table-header">
                  <th>Nome</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th>
                    <input type="text" onChange={(e) => setNome(e.target.value)} required />
                  </th>
                </tr>
              </tbody>
            </table>
          </div>
          <div>
            <button type="submit" className="green-btn">
              Salvar
            </button>
          </div>
        </form>
        <button className="gray-btn" onClick={props.closePopup}>
          Cancelar
        </button>
      </div>
    </div>
  );
}

function EditarVia(props) {
  const [showConfirmationPopup, setShowConfirmationPopup] = useState(false);
  //Campos do Formulario
  const [nome, setNome] = useState(props.via.nome);

  const { addToast } = useToasts();

  function handleSubmit(event) {
    event.preventDefault();
    let via = {
      id: props.via.id,
      nome: nome,
    };

    Requests.putVia(via, storage.session.getItem('jwtToken'))
      .then((response) => {
        if (response.status === 200) {
          addToast('Via editada com sucesso', {
            appearance: 'success',
            autoDismiss: true,
          });
          props.updateVias();
          props.closePopup();
        } else {
          addToast(response.response.data.errors, {
            appearance: 'error',
            autoDismiss: true,
          });
        }
      })
      .catch(function (err) {
        console.log(err);
      });
  }

  function toggleConfirmDeletePopup() {
    setShowConfirmationPopup(!showConfirmationPopup);
  }

  function handleDelete() {
    Requests.deleteVia(props.via.id, storage.session.getItem('jwtToken'))
      .then((response) => {
        if (response.status === 200) {
          addToast('Via deletada com sucesso', {
            appearance: 'success',
            autoDismiss: true,
          });
          props.updateVias();
          props.closePopup();
        } else {
          addToast(response.response.data.errors, {
            appearance: 'error',
            autoDismiss: true,
          });
        }
      })
      .catch(function (err) {
        console.log(err);
      });
  }

  return (
    <div className="popup-container">
      <div className="popup" style={{ maxWidth: '40rem' }}>
        <div className="popup-header">
          <p>Editar Via</p>
        </div>
        <form onSubmit={handleSubmit}>
          <div className="popup-content">
            <table className="popup-table">
              <thead>
                <tr className="popup-table-header">
                  <th>Nome</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th>
                    <input type="text" defaultValue={props.via.nome} onChange={(e) => setNome(e.target.value)} required />
                  </th>
                </tr>
              </tbody>
            </table>
          </div>
          <div>
            <button type="button" className="red-btn" onClick={toggleConfirmDeletePopup}>
              Deletar
            </button>
            <button type="submit" className="green-btn">
              Salvar
            </button>
          </div>
        </form>
        <button className="gray-btn" onClick={props.closePopup}>
          Cancelar
        </button>
      </div>
      {showConfirmationPopup ? <ConfirmDelete deleteFunction={handleDelete} closePopup={toggleConfirmDeletePopup} /> : null}
    </div>
  );
}

function AdicionarMarca(props) {
  //Campos do Formulario
  const [nome, setNome] = useState('');

  const { addToast } = useToasts();

  function handleSubmit(event) {
    event.preventDefault();
    let marca = {
      nome: nome,
    };
    Requests.postMarca(marca, storage.session.getItem('jwtToken'))
      .then((response) => {
        if (response.status === 201) {
          addToast('Marca adicionada com sucesso', {
            appearance: 'success',
            autoDismiss: true,
          });
          props.updateMarcas();
          props.closePopup();
        } else {
          addToast(response.response.data.errors, {
            appearance: 'error',
            autoDismiss: true,
          });
        }
      })
      .catch(function (err) {
        console.log(err);
      });
  }

  return (
    <div className="popup-container">
      <div className="popup" style={{ maxWidth: '40rem' }}>
        <div className="popup-header">
          <p>Nova Marca de Ração</p>
        </div>
        <form onSubmit={handleSubmit}>
          <div className="popup-content">
            <table className="popup-table">
              <thead>
                <tr className="popup-table-header">
                  <th>Nome</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th>
                    <input type="text" onChange={(e) => setNome(e.target.value)} required />
                  </th>
                </tr>
              </tbody>
            </table>
          </div>
          <div>
            <button type="submit" className="green-btn">
              Salvar
            </button>
          </div>
        </form>
        <button className="gray-btn" onClick={props.closePopup}>
          Cancelar
        </button>
      </div>
    </div>
  );
}

function EditarMarca(props) {
  const [showConfirmationPopup, setShowConfirmationPopup] = useState(false);
  //Campos do Formulario
  const [nome, setNome] = useState(props.marca.nome);

  const { addToast } = useToasts();

  function handleSubmit(event) {
    event.preventDefault();
    let marca = {
      id: props.marca.id,
      nome: nome,
    };

    Requests.putMarca(marca, storage.session.getItem('jwtToken'))
      .then((response) => {
        if (response.status === 200) {
          addToast('Marca editada com sucesso', {
            appearance: 'success',
            autoDismiss: true,
          });
          props.updateMarcas();
          props.closePopup();
        } else {
          addToast(response.response.data.errors, {
            appearance: 'error',
            autoDismiss: true,
          });
        }
      })
      .catch(function (err) {
        console.log(err);
      });
  }

  function toggleConfirmDeletePopup() {
    setShowConfirmationPopup(!showConfirmationPopup);
  }

  function handleDelete() {
    Requests.deleteMarca(props.marca.id, storage.session.getItem('jwtToken'))
      .then((response) => {
        if (response.status === 200) {
          addToast('Marca deletada com sucesso', {
            appearance: 'success',
            autoDismiss: true,
          });
          props.updateMarcas();
          props.closePopup();
        } else {
          addToast(response.response.data.errors, {
            appearance: 'error',
            autoDismiss: true,
          });
        }
      })
      .catch(function (err) {
        console.log(err);
      });
  }

  return (
    <div className="popup-container">
      <div className="popup" style={{ maxWidth: '40rem' }}>
        <div className="popup-header">
          <p>Editar Marca</p>
        </div>
        <form onSubmit={handleSubmit}>
          <div className="popup-content">
            <table className="popup-table">
              <thead>
                <tr className="popup-table-header">
                  <th>Nome</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th>
                    <input type="text" defaultValue={props.marca.nome} onChange={(e) => setNome(e.target.value)} required />
                  </th>
                </tr>
              </tbody>
            </table>
          </div>
          <div>
            <button type="button" className="red-btn" onClick={toggleConfirmDeletePopup}>
              Deletar
            </button>
            <button type="submit" className="green-btn">
              Salvar
            </button>
          </div>
        </form>
        <button className="gray-btn" onClick={props.closePopup}>
          Cancelar
        </button>
      </div>
      {showConfirmationPopup ? <ConfirmDelete deleteFunction={handleDelete} closePopup={toggleConfirmDeletePopup} /> : null}
    </div>
  );
}

function AdicionarEspecie(props) {
  //Campos do Formulario
  const [nome, setNome] = useState('');

  const { addToast } = useToasts();

  function handleSubmit(event) {
    event.preventDefault();
    let especie = {
      nome: nome,
    };
    Requests.postEspecie(especie, storage.session.getItem('jwtToken'))
      .then((response) => {
        if (response.status === 201) {
          addToast('Espécie adicionada com sucesso', {
            appearance: 'success',
            autoDismiss: true,
          });
          props.updateEspecies();
          props.closePopup();
        } else {
          addToast(response.response.data.errors, {
            appearance: 'error',
            autoDismiss: true,
          });
        }
      })
      .catch(function (err) {
        console.log(err);
      });
  }

  return (
    <div className="popup-container">
      <div className="popup" style={{ maxWidth: '40rem' }}>
        <div className="popup-header">
          <p>Nova Espécie</p>
        </div>
        <form onSubmit={handleSubmit}>
          <div className="popup-content">
            <table className="popup-table">
              <thead>
                <tr className="popup-table-header">
                  <th>Nome</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th>
                    <input type="text" onChange={(e) => setNome(e.target.value)} required />
                  </th>
                </tr>
              </tbody>
            </table>
          </div>
          <div>
            <button type="submit" className="green-btn">
              Salvar
            </button>
          </div>
        </form>
        <button className="gray-btn" onClick={props.closePopup}>
          Cancelar
        </button>
      </div>
    </div>
  );
}

function EditarEspecie(props) {
  const [showConfirmationPopup, setShowConfirmationPopup] = useState(false);
  //Campos do Formulario
  const [nome, setNome] = useState(props.especie.nome);

  const { addToast } = useToasts();

  function handleSubmit(event) {
    event.preventDefault();
    let especie = {
      id: props.especie.id,
      nome: nome,
    };

    Requests.putEspecie(especie, storage.session.getItem('jwtToken'))
      .then((response) => {
        if (response.status === 200) {
          addToast('Especie editada com sucesso', {
            appearance: 'success',
            autoDismiss: true,
          });
          props.updateEspecies();
          props.closePopup();
        } else {
          addToast(response.response.data.errors, {
            appearance: 'error',
            autoDismiss: true,
          });
        }
      })
      .catch(function (err) {
        console.log(err);
      });
  }

  function toggleConfirmDeletePopup() {
    setShowConfirmationPopup(!showConfirmationPopup);
  }

  function handleDelete() {
    Requests.deleteEspecie(props.especie.id, storage.session.getItem('jwtToken'))
      .then((response) => {
        if (response.status === 200) {
          addToast('Especie deletada com sucesso', {
            appearance: 'success',
            autoDismiss: true,
          });
          props.updateEspecies();
          props.closePopup();
        } else {
          addToast(response.response.data.errors, {
            appearance: 'error',
            autoDismiss: true,
          });
        }
      })
      .catch(function (err) {
        console.log(err);
      });
  }

  return (
    <div className="popup-container">
      <div className="popup" style={{ maxWidth: '40rem' }}>
        <div className="popup-header">
          <p>Editar Especie</p>
        </div>
        <form onSubmit={handleSubmit}>
          <div className="popup-content">
            <table className="popup-table">
              <thead>
                <tr className="popup-table-header">
                  <th>Nome</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th>
                    <input type="text" defaultValue={props.especie.nome} onChange={(e) => setNome(e.target.value)} required />
                  </th>
                </tr>
              </tbody>
            </table>
          </div>
          <div>
            <button type="button" className="red-btn" onClick={toggleConfirmDeletePopup}>
              Deletar
            </button>
            <button type="submit" className="green-btn">
              Salvar
            </button>
          </div>
        </form>
        <button className="gray-btn" onClick={props.closePopup}>
          Cancelar
        </button>
      </div>
      {showConfirmationPopup ? <ConfirmDelete deleteFunction={handleDelete} closePopup={toggleConfirmDeletePopup} /> : null}
    </div>
  );
}

const customStyles = {
  container: (provided, state) => ({
    ...provided,
    margin: '0px 0px',
  }),
  indicatorSeparator: () => null,
  control: (provided, state) => ({
    ...provided,
    whiteSpace: 'nowrap',
    color: 'rgb(60, 60, 60)',
    textAlign: 'center',
    boxShadow: '0px 0px 20px rgba(0, 0, 0, 0.2)',
    borderRadius: '8px',
    background: '#fff',
    spacing: '10px',
    maxWidth: '100%',
    width: '225px',
    //height: '2rem',
    border: 'none',
  }),
  option: (provided, state) => ({
    ...provided,
    color: 'rgb(60, 60, 60)',
  }),
  placeholder: (provided, state) => ({
    ...provided,
    textAlign: 'center',
    color: 'rgb(100, 100, 100)',
  }),
};

function customTheme(theme) {
  return {
    ...theme,
    colors: {
      ...theme.colors,
      primary75: 'rgb(187, 221, 211)',
      primary25: 'rgb(225, 255, 241)',
      primary: 'rgb(187, 221, 211)',
    },
  };
}

export {
  AdicionarMedicamento,
  EditarMedicamento,
  AdicionarRacao,
  EditarRacao,
  AdicionarFrequencia,
  AdicionarVia,
  AdicionarMarca,
  AdicionarEspecie,
  EditarFrequencia,
  EditarVia,
  EditarMarca,
  EditarEspecie,
};
