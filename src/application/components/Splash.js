import React from 'react';
import Lottie from 'react-lottie'

import splashFile from '../assets/images/dog'

export default class Splash extends React.PureComponent {

    state = {
        loading: true,
        lottieOptions: {
            loop: true,
            autoplay: true,
            animationData: splashFile,
            rendererSettings: {
                preserveAspectRatio: 'xMidYMid slice'
            }
        }
    }

    render() {
        return (
            <Lottie options={this.state.lottieOptions}
                height={500}
                width={500}
                isStopped={false}
                isPaused={false}
            />
        )
    }
}

