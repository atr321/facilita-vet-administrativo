import React from 'react';
import { FaBars } from 'react-icons/fa';
import '../styles/Header.css';
import * as storage from '../consts/storage.js';
import { NavLink, withRouter } from 'react-router-dom';

class Header extends React.Component {
  handleLogOut() {
    storage.session.removeItem('loggedInStatus');
    storage.session.removeItem('user');
    storage.session.removeItem('jwtToken');
  }

  render() {
    return (
      <div className="Header">
        <input type="checkbox" id="nav-check"></input>
        <div className="Logo-img-component">
          <NavLink exact to="/home">
            <img
              className="Logo-img"
              src={require('../assets/images/facilitavet-white.png')}
              alt="logo"
            />
          </NavLink>
        </div>
        <div className="nav-btn">
          <label htmlFor="nav-check">
            <FaBars className="FaBars" />
          </label>
        </div>
        <div className="Items">
          <NavLink
            className="navItem"
            activeClassName="selectedNavItem"
            exact
            to="/medicamentos"
          >
            Medicamentos
          </NavLink>
          <NavLink
            className="navItem"
            activeClassName="selectedNavItem"
            exact
            to="/racoes"
          >
            Rações
          </NavLink>
          <NavLink
            className="navItem"
            activeClassName="selectedNavItem"
            exact
            to="/outros"
          >
            Outros
          </NavLink>
          <NavLink
            className="navItem"
            activeClassName="selectedNavItem"
            exact
            to="/"
            onClick={this.handleLogOut}
          >
            Sair
          </NavLink>
        </div>
      </div>
    );
  }
}

export default withRouter(Header);
