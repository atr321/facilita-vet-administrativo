import axios from 'axios';
import * as CONSTS from '../consts/consts.js';

const requests = {
  getMedicamentos: function () {
    return this.axiosGET(`${CONSTS.URL_BASE}${CONSTS.URL_MEDICAMENTO}`);
  },

  postMedicamento: function (medicamento, token) {

    return axios
      .post(
        `${CONSTS.URL_BASE}${CONSTS.URL_MEDICAMENTO}`,
        {
          nome: medicamento.nome,
          dose_min: medicamento.dose_min === '' ? null : medicamento.dose_min,
          dose_max: medicamento.dose_max === '' ? null : medicamento.dose_max,
          dose_unica: medicamento.dose_unica === '' ? null : medicamento.dose_unica,
          concentracao: medicamento.concentracao === '' ? null : medicamento.concentracao,
          especie_id: medicamento.especie_id,
          unidade_medida_id: medicamento.unidade_medida_id,
          Vias: medicamento.Vias,
          Frequencias: medicamento.Frequencias,
        },
        {
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
        }
      )
      .then((response) => {
        return response;
      })
      .catch((error) => {
        return error;
      });
  },

  putMedicamento: function (medicamento, token) {
    
    return axios
      .put(
        `${CONSTS.URL_BASE}${CONSTS.URL_MEDICAMENTO}/${medicamento.id}`,
        {
          nome: medicamento.nome,
          dose_min: medicamento.dose_min === '' ? null : medicamento.dose_min,
          dose_max: medicamento.dose_max === '' ? null : medicamento.dose_max,
          dose_unica: medicamento.dose_unica === '' ? null : medicamento.dose_unica,
          concentracao: medicamento.concentracao === '' ? null : medicamento.concentracao,
          especie_id: medicamento.especie_id,
          unidade_medida_id: medicamento.unidade_medida_id,
          Vias: medicamento.Vias,
          Frequencias: medicamento.Frequencias,
        },
        {
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
        }
      )
      .then((response) => {
        return response;
      })
      .catch((error) => {
        return error;
      });
  },

  deleteMedicamento: function (medicamentoID, token) {
    return axios
      .delete(`${CONSTS.URL_BASE}${CONSTS.URL_MEDICAMENTO}/${medicamentoID}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((response) => {
        return response;
      })
      .catch((error) => {
        return error;
      });
  },

  getRacoes: function () {
    return this.axiosGET(`${CONSTS.URL_BASE}${CONSTS.URL_RACAO}`);
  },

  postRacao: function (racao, token) {
    return axios
      .post(
        `${CONSTS.URL_BASE}${CONSTS.URL_RACAO}`,
        {
          nome: racao.nome,
          energia_metabolizavel: racao.energia_metabolizavel,
          marca_racao_id: racao.marca_racao_id,
          especie_id: racao.especie_id,
        },
        {
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
        }
      )
      .then((response) => {
        return response;
      })
      .catch((error) => {
        return error;
      });
  },

  putRacao: function (racao, token) {
    return axios
      .put(
        `${CONSTS.URL_BASE}${CONSTS.URL_RACAO}/${racao.id}`,
        {
          nome: racao.nome,
          energia_metabolizavel: racao.energia_metabolizavel,
          marca_racao_id: racao.marca_racao_id,
          especie_id: racao.especie_id,
        },
        {
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
        }
      )
      .then((response) => {
        return response;
      })
      .catch((error) => {
        return error;
      });
  },

  deleteRacao: function (racaoID, token) {
    return axios
      .delete(`${CONSTS.URL_BASE}${CONSTS.URL_RACAO}/${racaoID}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((response) => {
        return response;
      })
      .catch((error) => {
        return error;
      });
  },

  getMarcas: function () {
    return this.axiosGET(`${CONSTS.URL_BASE}${CONSTS.URL_MARCA_RACAO}`);
  },

  postMarca: function (marca, token) {
    return axios
      .post(
        `${CONSTS.URL_BASE}${CONSTS.URL_MARCA_RACAO}`,
        {
          nome: marca.nome,
        },
        {
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
        }
      )
      .then((response) => {
        return response;
      })
      .catch((error) => {
        return error;
      });
  },

  putMarca: function (marca, token) {
    return axios
      .put(
        `${CONSTS.URL_BASE}${CONSTS.URL_MARCA_RACAO}/${marca.id}`,
        {
          nome: marca.nome,
        },
        {
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
        }
      )
      .then((response) => {
        return response;
      })
      .catch((error) => {
        return error;
      });
  },

  deleteMarca: function (marcaID, token) {
    return axios
      .delete(`${CONSTS.URL_BASE}${CONSTS.URL_MARCA_RACAO}/${marcaID}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((response) => {
        console.log(response)
        return response;
      })
      .catch((error) => {
        return error;
      });
  },

  getUnidades_medida: function () {
    return this.axiosGET(`${CONSTS.URL_BASE}${CONSTS.URL_UNIDADE_MEDIDA}`);
  },

  getEspecies: function () {
    return this.axiosGET(`${CONSTS.URL_BASE}${CONSTS.URL_ESPECIE}`);
  },

  postEspecie: function (especie, token) {
    return axios
      .post(
        `${CONSTS.URL_BASE}${CONSTS.URL_ESPECIE}`,
        {
          nome: especie.nome,
        },
        {
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
        }
      )
      .then((response) => {
        return response;
      })
      .catch((error) => {
        return error;
      });
  },

  putEspecie: function (especie, token) {
    return axios
      .put(
        `${CONSTS.URL_BASE}${CONSTS.URL_ESPECIE}/${especie.id}`,
        {
          nome: especie.nome,
        },
        {
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
        }
      )
      .then((response) => {
        return response;
      })
      .catch((error) => {
        return error;
      });
  },

  deleteEspecie: function (especieID, token) {
    return axios
      .delete(`${CONSTS.URL_BASE}${CONSTS.URL_ESPECIE}/${especieID}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((response) => {
        console.log(response)
        return response;
      })
      .catch((error) => {
        return error;
      });
  },

  getVias: function () {
    return this.axiosGET(`${CONSTS.URL_BASE}${CONSTS.URL_VIA}`);
  },

  postVia: function (via, token) {
    return axios
      .post(
        `${CONSTS.URL_BASE}${CONSTS.URL_VIA}`,
        {
          nome: via.nome,
        },
        {
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
        }
      )
      .then((response) => {
        return response;
      })
      .catch((error) => {
        return error;
      });
  },

  putVia: function (via, token) {
    return axios
      .put(
        `${CONSTS.URL_BASE}${CONSTS.URL_VIA}/${via.id}`,
        {
          nome: via.nome,
        },
        {
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
        }
      )
      .then((response) => {
        return response;
      })
      .catch((error) => {
        return error;
      });
  },

  deleteVia: function (viaID, token) {
    return axios
      .delete(`${CONSTS.URL_BASE}${CONSTS.URL_VIA}/${viaID}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((response) => {
        console.log(response)
        return response;
      })
      .catch((error) => {
        return error;
      });
  },

  getFrequencias: function () {
    return this.axiosGET(`${CONSTS.URL_BASE}${CONSTS.URL_FREQUENCIA}`);
  },

  postFrequencia: function (frequencia, token) {
    return axios
      .post(
        `${CONSTS.URL_BASE}${CONSTS.URL_FREQUENCIA}`,
        {
          nome: frequencia.nome,
        },
        {
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
        }
      )
      .then((response) => {
        return response;
      })
      .catch((error) => {
        return error;
      });
  },

  putFrequencia: function (frequencia, token) {
    return axios
      .put(
        `${CONSTS.URL_BASE}${CONSTS.URL_FREQUENCIA}/${frequencia.id}`,
        {
          nome: frequencia.nome,
        },
        {
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
        }
      )
      .then((response) => {
        return response;
      })
      .catch((error) => {
        return error;
      });
  },

  deleteFrequencia: function (frequenciaID, token) {
    return axios
      .delete(`${CONSTS.URL_BASE}${CONSTS.URL_FREQUENCIA}/${frequenciaID}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((response) => {
        console.log(response)
        return response;
      })
      .catch((error) => {
        return error;
      });
  },

  getUser: function (userID, token) {
    return axios
      .get(`${CONSTS.URL_BASE}${CONSTS.URL_USER}/${userID}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((response) => {
        return response.data;
      })
      .catch((error) => console.log(error));
  },

  axiosGET: function (url) {
    return axios
      .get(url)
      .then((response) => {
        return response.data;
      })
      .catch((error) => console.log(error));
  },
};

export default requests;
