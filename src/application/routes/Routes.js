import React, { Component } from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import * as storage from '../consts/storage.js';
import Header from '../components/Header';
import Medicamentos from '../screens/Medicamentos.js';
import App from '../../App';
import Login from '../screens/Login.js';
import Racoes from '../screens/Racoes.js';
import Welcome from '../screens/Welcome.js';
import Outros from '../screens/Outros.js';

const ProtectedRoute = ({ component: Component, ...rest }) => {
  return (
    <Route
      {...rest}
      render={(props) => {
        if (storage.session.getItem('loggedInStatus')) {
          return <Component {...props} />;
        } else {
          return (
            <Redirect
              to={{
                pathname: '/',
                state: {
                  from: props.location,
                },
              }}
            />
          );
        }
      }}
    />
  );
};

export default class Routes extends Component {
  render() {
    return (
      <BrowserRouter basename={process.env.PUBLIC_URL}>
        <App>
          <ProtectedRoute
            path={['/home', '/medicamentos', '/racoes', '/outros']}
            exact
            component={Header}
          />
          <Switch>
            <Route exact path="/" component={Login} />
            <ProtectedRoute exact path="/home" component={Welcome} />
            <ProtectedRoute
              exact
              path="/medicamentos"
              component={Medicamentos}
            />
            <ProtectedRoute exact path="/racoes" component={Racoes} />
            <ProtectedRoute exact path="/outros" component={Outros} />
          </Switch>
        </App>
      </BrowserRouter>
    );
  }
}
