import React, { useState, useEffect } from 'react';
import Requests from '../requests/Requests.js';
import '../styles/Welcome.css';
import * as storage from '../consts/storage.js';

export default function Welcome() {
  const [nome, setNome] = useState('');

  useEffect(() => {
    getUserName(storage.session.getItem('user'));
  }, []);

  function getUserName(user) {
    Requests.getUser(
      JSON.parse(user).id,
      storage.session.getItem('jwtToken')
    ).then((data) => {
      setNome(data.nome);
    });
  }

  return (
    <div>
      <div className="welcome-page">
        <div className="centeredContainer">
          <h1>Bem Vindo, {nome}</h1>
          <p>
            Aqui você poderá administrar os dados utilizados pelo nosso
            aplicativo, basta utilizar os botões acima.
          </p>
        </div>
      </div>
    </div>
  );
}
