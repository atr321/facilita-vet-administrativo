import React, { useState } from 'react';
import '../styles/Login.css';
import axios from 'axios';
import * as CONSTS from '../consts/consts.js';
import * as storage from '../consts/storage.js';
import { useToasts } from 'react-toast-notifications';

export default function Login(props) {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const { addToast } = useToasts();

  function handleSucessfulAuth(data) {
    storage.session.setItem('loggedInStatus', true);
    storage.session.setItem('user', JSON.stringify(data.user));
    storage.session.setItem('jwtToken', data.token);
    props.history.push('/home');
  }

  function handleSubmit(event) {
    axios
      .post(
        `${CONSTS.URL_BASE}${CONSTS.URL_LOGIN}`,
        {
          email: email,
          senha: password,
        },
        {
          headers: {
            'Content-Type': 'application/json',
          },
        }
      )
      .then((response) => {
        if (response.status === 200) {
          console.log('here');
          return handleSucessfulAuth(response.data);
        }
      })
      .catch((error) => {
        console.log('login error', error);
        addToast('Email ou Senha Inválidos', {
          appearance: 'error',
          autoDismiss: true,
        });
      });
    event.preventDefault();
  }

  return (
    <div className="container">
      <div className="window">
        <div className="overlay"></div>

        <div className="content">
          <div className="login-header">
            <div className="Logo-img-container">
              <img
                className="Logo-img"
                src={require('../assets/images/facilitavet-white.png')}
                alt="background"
              />
            </div>
            <div className="text">Admin</div>
          </div>

          <form onSubmit={handleSubmit}>
            <div>
              <input
                type="email"
                placeholder="Email"
                onChange={(e) => setEmail(e.target.value)}
                className="input-line"
                required
              ></input>
              <input
                type="password"
                placeholder="Senha"
                onChange={(e) => setPassword(e.target.value)}
                className="input-line"
                required
              ></input>
            </div>

            <button type="submit" className="round-btn">
              Login
            </button>
          </form>
        </div>
      </div>
    </div>
  );
}
