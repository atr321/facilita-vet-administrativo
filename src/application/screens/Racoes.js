import React from 'react';
import { FaSearch, FaPlus } from 'react-icons/fa';
import Requests from '../requests/Requests.js';
import { AdicionarRacao, EditarRacao } from '../components/Popups.js';
import '../../App.css';
import '../styles/TablePage.css';

export default class Home extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      racoes: [],
      showEditarPopup: false,
      showAdicionarPopup: false,
      selectedRacao: '',
      searchText: '',
    };
  }

  componentDidMount() {
    this.updateRacoes();
  }

  toggleEditarPopup(racao) {
    this.setState({
      showEditarPopup: !this.state.showEditarPopup,
      selectedRacao: racao,
    });
  }

  toggleAdicionarPopup() {
    this.setState({
      showAdicionarPopup: !this.state.showAdicionarPopup,
    });
  }

  updateRacoes() {
    Requests.getRacoes().then((data) => {
      this.setState({ racoes: data });
    });
  }

  handleSearchInput = (e) => {
    this.setState({searchText: e.target.value})

  }

  renderRows() {
    //filter by search input
    let filteredRows = this.state.racoes.filter((racao) => {
      let info = racao.nome + racao.energia_metabolizavel + racao.MarcaRacao.nome + racao.Especie.nome
      return info.toLowerCase().includes(this.state.searchText.toLowerCase())
    })

    //sort by alphabetical order
    filteredRows = filteredRows.sort(function(a, b) {
      return a.nome.toLowerCase().localeCompare(b.nome.toLowerCase());
    });

    try {
      return filteredRows.map((racao) => {
        return (
          <tr
            className="table-row"
            onClick={this.toggleEditarPopup.bind(this, racao)}
            key={racao.id}
          >
            <td>{racao.nome}</td>
            <td>{racao.energia_metabolizavel} </td>
            <td>{racao.MarcaRacao.nome}</td>
            <td>{racao.Especie.nome}</td>
          </tr>
        );
      });
    } catch (error) {
      console.log(error);
    }
  }

  render() {
    return (
      <div>
        <div className="container">
          <div className="function-bar">
            <div className="search-input">
              <div className="icon">
                <FaSearch className="fasearch" />
              </div>
              <input type="search" id="search" placeholder="Pesquisar" onChange={this.handleSearchInput}/>
            </div>
            <button
              onClick={this.toggleAdicionarPopup.bind(this)}
              className="add-button"
            >
              <FaPlus className="faplus" />
            </button>
          </div>

          <section className="table">
            <div className="table-header">
              <table>
                <thead>
                  <tr>
                    <th>Nome</th>
                    <th>Energia Metabolizável</th>
                    <th>Marca</th>
                    <th>Espécie</th>
                  </tr>
                </thead>
              </table>
            </div>
            <div className="table-content">
              <table>
                <tbody>{this.renderRows()}</tbody>
              </table>
            </div>
          </section>

          {this.state.showEditarPopup ? (
            <EditarRacao
              racao={this.state.selectedRacao}
              updateRacoes={this.updateRacoes.bind(this)}
              closePopup={this.toggleEditarPopup.bind(this)}
            />
          ) : null}
          {this.state.showAdicionarPopup ? (
            <AdicionarRacao
              racoes={this.state.racoes}
              updateRacoes={this.updateRacoes.bind(this)}
              closePopup={this.toggleAdicionarPopup.bind(this)}
            />
          ) : null}
        </div>
      </div>
    );
  }
}
