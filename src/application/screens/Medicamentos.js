import React from 'react';
import { FaSearch, FaPlus } from 'react-icons/fa';
import Requests from '../requests/Requests.js';
import {
  AdicionarMedicamento,
  EditarMedicamento,
} from '../components/Popups.js';
import '../../App.css';
import '../styles/TablePage.css';

export default class Medicamentos extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      meds: [],
      showEditarPopup: false,
      showAdicionarPopup: false,
      selectedMed: '',
      searchText: '',
    };
  }

  componentDidMount() {
    this.updateMedicamentos();
  }

  toggleEditarPopup(med) {
    this.setState({
      showEditarPopup: !this.state.showEditarPopup,
      selectedMed: med,
    });
  }

  toggleAdicionarPopup() {
    this.setState({
      showAdicionarPopup: !this.state.showAdicionarPopup,
    });
  }

  updateMedicamentos() {
    Requests.getMedicamentos().then((data) => {
      this.setState({ meds: data });
    });
  }

  handleSearchInput = (e) => {
    this.setState({searchText: e.target.value})

  }

  renderRows() {
    //filter by search input
    let filteredRows = this.state.meds.filter((med) => {
      let vias = ''
      med.Vias.map((via) => {return vias= vias + via.nome})
      let freqs = ''
      med.Frequencias.map((freq) => {return freqs = freqs + freq.nome})

      let info = med.nome + med.dose_min + med.dose_max + med.concentracao + vias + freqs + med.Especie.nome
      return info.toLowerCase().includes(this.state.searchText.toLowerCase())
    })

    //sort by alphabetical order
    filteredRows = filteredRows.sort(function(a, b) {
      return a.nome.toLowerCase().localeCompare(b.nome.toLowerCase());
    });

    try {
      return filteredRows.map((medicamento) => {
        let vias = ''
        medicamento.Vias.map((via) => {return vias = vias + ' | ' +via.nome})
        let freqs = ''
        medicamento.Frequencias.map((freq) => {return freqs = freqs +' | '+ freq.nome})

        let doseUnica = ''
        let doseMax = ''
        let doseMin = ''

        if(medicamento.dose_unica != null){
          doseUnica = "Única: " + medicamento.dose_unica + medicamento.UnidadeMedida.nome.toLowerCase()
        }
        if(medicamento.dose_min != null){
          doseMin = 'Min: ' + medicamento.dose_min + medicamento.UnidadeMedida.nome.toLowerCase()
        }
        if(medicamento.dose_max != null){
          doseMax = 'Max: ' + medicamento.dose_max + medicamento.UnidadeMedida.nome.toLowerCase()
        }

        return (
          <tr
            className="table-row"
            onClick={this.toggleEditarPopup.bind(this, medicamento)}
            key={medicamento.id}
          >
            <td>{medicamento.nome}</td>
            <td>
              {doseUnica !== '' && doseUnica}
              {doseMin !== '' && doseMin}
              <br/>
              {doseMax !== '' && doseMax}
            </td>
            <td>{medicamento.concentracao !== null && medicamento.concentracao + 'mg/ml'}</td>
            <td>
              {vias.substring(3)}
            </td>
            <td>
              {freqs.substring(3)}
            </td>
            <td>{medicamento.Especie.nome}</td>
          </tr>
        );
      });
    } catch (error) {
      console.log(error);
    }
  }

  render() {
    return (
      <div>
        <div className="container">
          <div className="function-bar">
            <div className="search-input">
              <div className="icon">
                <FaSearch className="fasearch" />
              </div>
              <input type="search" id="search" placeholder="Pesquisar" onChange={this.handleSearchInput} />
            </div>
            <button
              onClick={this.toggleAdicionarPopup.bind(this)}
              className="add-button"
            >
              <FaPlus className="faplus" />
            </button>
          </div>

          <section className="table">
            <div className="table-header">
              <table>
                <thead>
                  <tr>
                    <th>Nome</th>
                    <th>Dose</th>
                    <th>Concentração</th>
                    <th>VIA</th>
                    <th>FREQ</th>
                    <th>Espécie</th>
                  </tr>
                </thead>
              </table>
            </div>
            <div className="table-content">
              <table>
                <tbody>{this.renderRows()}</tbody>
              </table>
            </div>
          </section>

          {this.state.showEditarPopup ? (
            <EditarMedicamento
              medicamento={this.state.selectedMed}
              updateMedicamentos={this.updateMedicamentos.bind(this)}
              closePopup={this.toggleEditarPopup.bind(this)}
            />
          ) : null}
          {this.state.showAdicionarPopup ? (
            <AdicionarMedicamento
              updateMedicamentos={this.updateMedicamentos.bind(this)}
              closePopup={this.toggleAdicionarPopup.bind(this)}
            />
          ) : null}
        </div>
      </div>
    );
  }
}
