import React from 'react';
import { FaPlus } from 'react-icons/fa';
import Requests from '../requests/Requests.js';
import { AdicionarFrequencia, AdicionarVia, AdicionarMarca, AdicionarEspecie, EditarFrequencia, EditarVia, EditarMarca, EditarEspecie } from '../components/Popups.js';
import '../../App.css';
import '../styles/TablePage.css';

export default class Outros extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      especies: [],
      vias: [],
      frequencias: [],
      marcas: [],

      showEditarFrequenciaPopup: false,
      showAdicionarFrequenciaPopup: false,
      selectedFrequencia: '',

      showEditarViaPopup: false,
      showAdicionarViaPopup: false,
      selectedVia: '',

      showEditarMarcaPopup: false,
      showAdicionarMarcaPopup: false,
      selectedMarca: '',

      showEditarEspeciePopup: false,
      showAdicionarEspeciePopup: false,
      selectedEspecie: '',
    };
  }

  componentDidMount() {
    Requests.getEspecies().then((data) => {
      this.setState({ especies: data });
    });
    Requests.getVias().then((data) => {
      this.setState({ vias: data });
    });
    Requests.getFrequencias().then((data) => {
      this.setState({ frequencias: data });
    });
    Requests.getMarcas().then((data) => {
      this.setState({ marcas: data });
    });
  }

  toggleAdicionarFrequenciaPopup() {
    this.setState({
      showAdicionarFrequenciaPopup: !this.state.showAdicionarFrequenciaPopup,
    });
  }

  toggleEditarFrequenciaPopup(frequencia) {
    this.setState({
      showEditarFrequenciaPopup: !this.state.showEditarFrequenciaPopup,
      selectedFrequencia: frequencia,
    });
  }

  toggleAdicionarViaPopup() {
    this.setState({
      showAdicionarViaPopup: !this.state.showAdicionarViaPopup,
    });
  }

  toggleEditarViaPopup(via) {
    this.setState({
      showEditarViaPopup: !this.state.showEditarViaPopup,
      selectedVia: via,
    });
  }

  toggleAdicionarMarcaPopup() {
    this.setState({
      showAdicionarMarcaPopup: !this.state.showAdicionarMarcaPopup,
    });
  }

  toggleEditarMarcaPopup(marca) {
    this.setState({
      showEditarMarcaPopup: !this.state.showEditarMarcaPopup,
      selectedMarca: marca,
    });
  }

  toggleAdicionarEspeciePopup() {
    this.setState({
      showAdicionarEspeciePopup: !this.state.showAdicionarEspeciePopup,
    });
  }

  toggleEditarEspeciePopup(especie) {
    this.setState({
      showEditarEspeciePopup: !this.state.showEditarEspeciePopup,
      selectedEspecie: especie,
    });
  }

  updateFrequencias() {
    Requests.getFrequencias().then((data) => {
      this.setState({ frequencias: data });
    });
  }

  updateVias() {
    Requests.getVias().then((data) => {
      this.setState({ vias: data });
    });
  }

  updateMarcas() {
    Requests.getMarcas().then((data) => {
      this.setState({ marcas: data });
    });
  }

  updateEspecies() {
    Requests.getEspecies().then((data) => {
      this.setState({ especies: data });
    });
  }

  renderEspecies() {
    const especies = this.state.especies;
    let rows = [];

    let orderedRows = especies.sort(function(a, b) {
      return a.nome.toLowerCase().localeCompare(b.nome.toLowerCase());
    });

    try {
      orderedRows.forEach((especie) => {
        rows.push(
          <tr className="table-row" onClick={this.toggleEditarEspeciePopup.bind(this, especie)} key={especie.id}>
            <td>{especie.nome}</td>
          </tr>
        );
      });
    } catch (error) {
      console.log(error);
    }

    return rows;
  }

  renderVias() {
    const vias = this.state.vias;
    let rows = [];

    let orderedRows = vias.sort(function(a, b) {
      return a.nome.toLowerCase().localeCompare(b.nome.toLowerCase());
    });


    try {
      orderedRows.forEach((via) => {
        rows.push(
          <tr className="table-row" onClick={this.toggleEditarViaPopup.bind(this, via)} key={via.id}>
            <td>{via.nome}</td>
          </tr>
        );
      });
    } catch (error) {
      console.log(error);
    }
    return rows;
  }

  renderFrequencias() {

    let orderedRows = this.state.frequencias.sort(function(a, b) {
      return a.nome.toLowerCase().localeCompare(b.nome.toLowerCase());
    });

    try {
      return orderedRows.map((frequencia) => {
        return (
          <tr
            className="table-row"
            onClick={this.toggleEditarFrequenciaPopup.bind(this, frequencia)}
            key={frequencia.id}
          >
            <td>{frequencia.nome}</td>
          </tr>
        );
      });
    } catch (error) {
      console.log(error);
    }
  }

  renderMarcas() {
    const marcas = this.state.marcas;
    let rows = [];

    let orderedRows = marcas.sort(function(a, b) {
      return a.nome.toLowerCase().localeCompare(b.nome.toLowerCase());
    });

    try {
      orderedRows.forEach((marca) => {
        rows.push(
          <tr className="table-row" onClick={this.toggleEditarMarcaPopup.bind(this, marca)} key={marca.id}>
            <td>{marca.nome}</td>
          </tr>
        );
      });
    } catch (error) {
      console.log(error);
    }
    return rows;
  }

  render() {
    return (
      <div>
        <div className="outros-container">
          {/*FREQUENCIAS*/}

          <div className="container2">
            <section className="table">
              <div className="table-header">
                <table>
                  <thead>
                    <tr>
                      <th>Frequencias</th>
                    </tr>
                  </thead>
                </table>
                <button className="add-button2"
                  onClick={this.toggleAdicionarFrequenciaPopup.bind(this)}
                >
                  <FaPlus className="faplus" />
                </button>
              </div>
              <div className="table-content">
                <table>
                  <tbody>{this.renderFrequencias()}</tbody>
                </table>
              </div>
            </section>

            {this.state.showEditarFrequenciaPopup ? (
            <EditarFrequencia
              frequencia={this.state.selectedFrequencia}
              updateFrequencias={this.updateFrequencias.bind(this)}
              closePopup={this.toggleEditarFrequenciaPopup.bind(this)}
            />
          ) : null}
          {this.state.showAdicionarFrequenciaPopup ? (
            <AdicionarFrequencia
              updateFrequencias={this.updateFrequencias.bind(this)}
              closePopup={this.toggleAdicionarFrequenciaPopup.bind(this)}
            />
          ) : null}

          </div>

          {/*VIAS*/}

          <div className="container2">
            <section className="table">
              <div className="table-header">
                <table>
                  <thead>
                    <tr>
                      <th>Vias</th>
                    </tr>
                  </thead>
                </table>
                <button className="add-button2"
                  onClick={this.toggleAdicionarViaPopup.bind(this)}
                >
                  <FaPlus className="faplus" />
                </button>
              </div>
              <div className="table-content">
                <table>
                  <tbody>{this.renderVias()}</tbody>
                </table>
              </div>
            </section>
            {this.state.showEditarViaPopup ? (
            <EditarVia
              via={this.state.selectedVia}
              updateVias={this.updateVias.bind(this)}
              closePopup={this.toggleEditarViaPopup.bind(this)}
            />
          ) : null}
            {this.state.showAdicionarViaPopup ? (
            <AdicionarVia
              updateVias={this.updateVias.bind(this)}
              closePopup={this.toggleAdicionarViaPopup.bind(this)}
            />
          ) : null}

          </div>

          {/*MARCAS*/}

          <div className="container2">
            <section className="table">
              <div className="table-header">
                <table>
                  <thead>
                    <tr>
                      <th>Marcas de Ração</th>
                    </tr>
                  </thead>
                </table>
                <button className="add-button2"
                  onClick={this.toggleAdicionarMarcaPopup.bind(this)}
                >
                  <FaPlus className="faplus" />
                </button>
              </div>
              <div className="table-content">
                <table>
                  <tbody>{this.renderMarcas()}</tbody>
                </table>
              </div>
            </section>

            {this.state.showEditarMarcaPopup ? (
            <EditarMarca
              marca={this.state.selectedMarca}
              updateMarcas={this.updateMarcas.bind(this)}
              closePopup={this.toggleEditarMarcaPopup.bind(this)}
            />
          ) : null}
            {this.state.showAdicionarMarcaPopup ? (
            <AdicionarMarca
              updateMarcas={this.updateMarcas.bind(this)}
              closePopup={this.toggleAdicionarMarcaPopup.bind(this)}
            />
          ) : null}

          </div>

          {/*Especies*/}

          <div className="container2">
            <section className="table">
              <div className="table-header">
                <table>
                  <thead>
                    <tr>
                      <th>Espécies</th>
                    </tr>
                  </thead>
                </table>
                <button className="add-button2"
                  onClick={this.toggleAdicionarEspeciePopup.bind(this)}
                >
                  <FaPlus className="faplus" />
                </button>
              </div>
              <div className="table-content">
                <table>
                  <tbody>{this.renderEspecies()}</tbody>
                </table>
              </div>
            </section>

            {this.state.showEditarEspeciePopup ? (
            <EditarEspecie
              especie={this.state.selectedEspecie}
              updateEspecies={this.updateEspecies.bind(this)}
              closePopup={this.toggleEditarEspeciePopup.bind(this)}
            />
          ) : null}
            {this.state.showAdicionarEspeciePopup ? (
            <AdicionarEspecie
              updateEspecies={this.updateEspecies.bind(this)}
              closePopup={this.toggleAdicionarEspeciePopup.bind(this)}
            />
          ) : null}

          </div>
        </div>
      </div>
    );
  }
}
