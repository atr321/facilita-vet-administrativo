import React from 'react';
import ReactDOM from 'react-dom';
import { ToastProvider } from 'react-toast-notifications';
import './index.css';
import Routes from './application/routes/Routes.js';
import './application/assets/fonts/FredokaOne-Regular.ttf';
import './application/assets/fonts/Sriracha-Regular.ttf';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(
  <React.StrictMode>
    <ToastProvider autoDismissTimeout="3000" placement="bottom-right">
      <Routes />
    </ToastProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
