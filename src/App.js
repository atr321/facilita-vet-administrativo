import React from 'react';
import './App.css';

import Splash from './application/components/Splash';

export default class App extends React.Component {

  state = {
    splash: true
  }

  componentDidMount() {
    setTimeout(() => this.setState({ splash: false }), 1000);
  }

  render() {
    if (this.state.splash) {
      return (
        <div className="App-Splash" >
          <div className="App-loading" >
            <Splash />
          </div>
        </div>
      )
    }
    return (
      this.props.children
    );
  }
}